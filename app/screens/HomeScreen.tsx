import React, { useContext } from 'react';
import { Button, Image, StyleSheet, Text, View } from 'react-native';
import { RouteParams } from '../navigation/RootNavigator';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/core';

type ScreenNavigationProp<
    T extends keyof RouteParams> = StackNavigationProp<RouteParams, T>;

type ScreenRouteProp<T extends keyof RouteParams> = RouteProp<
    RouteParams,
    T
>;
type Props<T extends keyof RouteParams> = {
    route: ScreenRouteProp<T>;
    navigation: ScreenNavigationProp<T>;
};

const HomeScreen: React.FC<Props<'Home'>> = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <View style={styles.button}>
                <Button
                    onPress={() => navigation.navigate('Cat')}
                    title="J'aime les chats"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
            <View style={styles.button}>
                <Button
                    onPress={() => navigation.navigate('Dog')}
                    title="J'aime les chiens"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
            <View style={styles.button}>
                <Button
                    onPress={() => navigation.navigate('Pikachu')}
                    title="J'aime les Pikachu"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
            <View style={styles.button}>
                <Button
                    onPress={() => navigation.navigate('RedPanda')}
                    title="J'aime les Pandas Roux"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        marginBottom: 20
    },
    button: {
        padding: 10
    },
});
export default HomeScreen;